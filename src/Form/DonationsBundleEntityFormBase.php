<?php

namespace Drupal\donations\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DonationsBundleEntityFormBase extends BundleEntityFormBase {}
