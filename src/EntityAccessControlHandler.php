<?php

namespace Drupal\donations;

use Drupal\entity\EntityAccessControlHandler as BaseEntityAccessControlHandler;

/**
 * Controls access based on the Commerce entity permissions.
 *
 * @see \Drupal\donations\EntityPermissionProvider
 */
class EntityAccessControlHandler extends BaseEntityAccessControlHandler {}
