<?php

namespace Drupal\donations;

use Drupal\entity\EntityPermissionProvider as BaseEntityPermissionProvider;

/**
 * Provides Donation entity permissions.
 *
 * @see \Drupal\entity\EntityPermissionProvider
 */
class EntityPermissionProvider extends BaseEntityPermissionProvider {}
