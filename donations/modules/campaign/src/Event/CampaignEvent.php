<?php

namespace Drupal\campaign\Event;

use Drupal\campaign\Entity\CampaignInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the campaign event.
 *
 * @see \Drupal\campaign\Event\CampaignEvents
 */
class CampaignEvent extends Event {
  
  /**
   * The campaign.
   *
   * @var \Drupal\campaign\Entity\CampaignInterface
   */
  protected $campaign;

  /**
   * Constructs a new CampaignEvent.
   *
   * @param \Drupal\campaign\Entity\CampaignInterface $campaign
   *   The campaign.
   */
  public function __construct(CampaignInterface $campaign) {
    $this->campaign = $campaign;
  }

  /**
   * Gets the campaign.
   *
   * @return \Drupal\campaign\Entity\CampaignInterface
   *   The campaign.
   */
  public function getCampaign() {
    return $this->campaign;
  }

}
