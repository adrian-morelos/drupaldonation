<?php

namespace Drupal\campaign\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class CampaignForm extends ContentEntityForm
{
    /**
     * {@inheritdoc}
     */
    public function form(array $form, FormStateInterface $form_state)
    {
        // Disable caching on this form.
        $form_state->setCached(FALSE);

        // Donation Form Options Vertical Tab
        $form['donation_options'] = [
            '#type' => 'vertical_tabs',
            '#title' => t('Donation Form Options'),
            '#attributes' => ['class' => ['donation-options-information']],
        ];

        /** @var \Drupal\campaign\Entity\CampaignInterface $campaign */
        $campaign = $this->entity;
        $current_user = $this->currentUser();

        $form = parent::form($form, $form_state);

        $form['#tree'] = TRUE;
        $form['#theme'] = ['campaign_form'];
        $form['#attached']['library'][] = 'campaign/drupal.campaign';

        // Donation Options Tab
        $this->buildDonationOptionTabForm($form, $form_state);
        // Display Options Tab
        $this->buildDisplayOptionTabForm($form, $form_state);
        // Donation Goal Tab
        $this->buildDonationGoalTabForm($form, $form_state);
        // Form Content Tab
        $this->buildFormContentTabForm($form, $form_state);

        // Campaign author information for administrators.
        $form['author'] = [
            '#type' => 'details',
            '#title' => t('Authoring information'),
            '#group' => 'donation_options',
            '#attributes' => array(
                'class' => array('campaign-form-author'),
            ),
            '#weight' => 90,
            '#optional' => FALSE,
            '#attached' => array(
                'library' => array('campaign/drupal.campaign'),
            ),
        ];

        if (isset($form['uid'])) {
            $form['uid']['#group'] = 'author';
        }

        if (isset($form['created'])) {
            $form['created']['#group'] = 'author';
        }

        // Changed must be sent to the client, for later overwrite error checking.
        $form['changed'] = [
            '#type' => 'hidden',
            '#default_value' => $campaign->getChangedTime(),
        ];

        if ($this->operation == 'edit') {
            //$form['#title'] = $this->t('<em>Edit @type</em> @title', array('@type' => node_get_type_label($campaign), '@title' => $campaign->label()));
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        //ksm($form_state->getValues());
        $campaign = $this->getEntity();

        $donation_option = $form_state->getValue(['form_field_options_tab', 'donation_option']);
        $campaign->setDonationOption($donation_option);

        $set_price = $form_state->getValue(['form_field_options_tab', 'set_price']);
        $campaign->setPrice($set_price);

        $custom_amount = $form_state->getValue(['form_field_options_tab', 'custom_amount']);
        $custom_amount_value = ($custom_amount == 'enabled');
        $campaign->setCustomAmount($custom_amount_value);
        if($custom_amount_value){
            $custom_amount_minimum = $form_state->getValue(['form_field_options_tab', 'custom_amount_minimum']);
            $campaign->setMinimumAmount($custom_amount_minimum);

            $custom_amount_text = $form_state->getValue(['form_field_options_tab', 'custom_amount_text']);
            $campaign->setCustomAmountText($custom_amount_text);
        }else{
            $campaign->setMinimumAmount(NULL);
            $campaign->setCustomAmountText(NULL);
        }

        $display_option = $form_state->getValue(['form_display_options_tab', 'display_option']);
        $campaign->setDisplayOption($display_option);

        $continue_button_label = $form_state->getValue(['form_display_options_tab', 'continue_button_label']);
        $campaign->setContinueButtonLabel($continue_button_label);

        $checkout_button_label = $form_state->getValue(['form_display_options_tab', 'checkout_button_label']);
        $campaign->setCheckoutButtonLabel($checkout_button_label);

        $default_gateway = $form_state->getValue(['form_display_options_tab', 'default_gateway']);
        $campaign->setDefaultGateway($default_gateway);

        $logged_in_only = $form_state->getValue(['form_display_options_tab', 'logged_in_only']);
        $logged_in_only_value = ($logged_in_only == 'enabled');
        $campaign->setGuestDonation($logged_in_only_value);

        $show_register_form = $form_state->getValue(['form_display_options_tab', 'show_register_form']);
        $campaign->setRegistrationForm($show_register_form);

        $floating_label= $form_state->getValue(['form_display_options_tab', 'floating_labels']);
        $campaign->setFloatingLabel($floating_label);

        $donation_goal = $form_state->getValue(['form_donation_goal_tab', 'donation_goal']);
        $donation_goal_value = ($donation_goal == 'enabled');
        $campaign->setDonationGoal($donation_goal_value);

        if($donation_goal_value){
            $goal_amount = $form_state->getValue(['form_donation_goal_tab', 'goal_amount']);
            $campaign->setGoalAmount($goal_amount);

            $goal_format = $form_state->getValue(['form_donation_goal_tab', 'goal_format']);
            $campaign->setGoalFormat($goal_format);

            $goal_progress_bar_color = $form_state->getValue(['form_donation_goal_tab', 'goal_progress_bar_color']);
            $campaign->setProgressBarColor($goal_progress_bar_color);

            $close_form_when_goal_achieved = $form_state->getValue(['form_donation_goal_tab', 'close_form_when_goal_achieved']);
            $campaign->setCloseFormConfig($close_form_when_goal_achieved);

            $goal_achieved_message = $form_state->getValue(['form_donation_goal_tab', 'goal_achieved_message']);
            $campaign->setCloseFormConfig($goal_achieved_message);
        }else{
            $campaign->setGoalAmount(NULL);
            $campaign->setGoalFormat('amount');
            $campaign->setCloseFormConfig('disabled');
            $campaign->setCloseFormConfig(NULL);
        }

        $display_content = $form_state->getValue(['form_content_tab', 'display_content']);
        $display_content_value = ($display_content == 'enabled');
        $campaign->setDisplayContent($display_content_value);
        if($display_content_value){
            $content_placement = $form_state->getValue(['form_content_tab', 'content_placement']);
            $campaign->setContentPlacement($content_placement);

            $form_content = $form_state->getValue(['form_content_tab', 'form_content']);
            $campaign->setFormContent($form_content);

        }else{
            $campaign->setContentPlacement('above');
            $campaign->setFormContent(NULL);
        }

        $status = $campaign->save();
        $t_args = array('%name' => $campaign->label());

        if ($status == SAVED_UPDATED) {
            drupal_set_message(t('The campaign %name has been updated.', $t_args));
        } elseif ($status == SAVED_NEW) {
            drupal_set_message(t('The campaign %name has been added.', $t_args));
        }
        if ($campaign->id()) {
            $form_state->setValue('cid', $campaign->id());
            $form_state->set('cid', $campaign->id());
            $form_state->setRedirect(
                'entity.campaign.canonical',
                array('campaign' => $campaign->id())
            );
        } else {
            // In the unlikely case something went wrong on save, the $campaign will be
            // rebuilt and $campaign form redisplayed the same way as in preview.
            drupal_set_message(t('The post could not be saved.'), 'error');
            $form_state->setRebuild();
        }
    }

    /**
     * Builds the Donation Option Tab form.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     */
    public function buildDonationOptionTabForm(array &$form, FormStateInterface $form_state)
    {
        /** @var \Drupal\campaign\Entity\CampaignInterface $campaign */
        $campaign = $this->entity;
        // Donation Option Tab
        $form['form_field_options_tab'] = [
            '#type' => 'details',
            '#group' => 'donation_options',
            '#title' => t('Donation Option'),
            '#attributes' => array(
                'class' => array('campaign-form-donation-option-tab'),
            ),
            '#weight' => 20,
            '#optional' => FALSE,
        ];
        // Donation Option
        $form['form_field_options_tab']['donation_option'] = [
            '#type' => 'select',
            '#title' => t('Donation Option'),
            '#default_value' => $campaign->getDonationOption(),
            '#options' => $campaign->getDefaultDonationOptions(),
            '#description' => t('Do you want this form to have one set donation price or multiple levels (for example, $10, $20, $50)?'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'class' => array('container-inline'),
            ),
            '#ajax' => [
                'callback' => '::donationOptionCallbackAjax',
                'wrapper' => 'multi-level-donation-wrapper',
            ],
        ];
        // Multi Donation Levels
        $this->buildMultiLevelDonationForm($form, $form_state);
        // Set Donation
        $form['form_field_options_tab']['set_price'] = [
            '#type' => 'textfield',
            '#title' => t('Set Donation'),
            '#default_value' => $campaign->getPrice(),
            '#size' => 10,
            '#maxlength' => 128,
            '#required' => FALSE,
            '#description' => t('This is the set donation amount for this form. If you have a "Custom Amount Minimum" set, make sure it is less than this amount.'),
            '#field_prefix' => t('$'),
            '#prefix' => '<div class="field-wrap currency-symbol-field">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'placeholder' => array('0.00'),
            ),
            '#states' => array(
                // Action to take.
                'invisible' => array(
                    'select[name="form_field_options_tab[donation_option]"]' => ['value' => 'multi'],
                ),
            ),
        ];
        // Custom Amount
        $custom_amount_val = $campaign->getCustomAmount();
        $custom_amount = ($custom_amount_val) ? 'enabled' : 'disabled';
        $form['form_field_options_tab']['custom_amount'] = [
            '#type' => 'radios',
            '#title' => t('Custom Amount'),
            '#default_value' => $custom_amount,
            '#options' => $campaign->getDefaultCustomAmountOptions(),
            '#description' => t('Do you want the user to be able to input their own donation amount?'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'class' => array('container-inline'),
            ),
        ];
        // Minimum Amount
        $form['form_field_options_tab']['custom_amount_minimum'] = [
            '#type' => 'textfield',
            '#title' => t('Minimum Amount'),
            '#default_value' => $campaign->getMinimumAmount(),
            '#size' => 10,
            '#maxlength' => 128,
            '#required' => FALSE,
            '#description' => t('Enter the minimum custom donation amount.'),
            '#field_prefix' => t('$'),
            '#prefix' => '<div class="field-wrap currency-symbol-field">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'placeholder' => array('0.00'),
            ),
            '#states' => array(
                // Action to take.
                'visible' => array(
                    'input[name="form_field_options_tab[custom_amount]"]' => ['value' => 'enabled'],
                ),
            ),
        ];
        // Custom Amount Text
        $form['form_field_options_tab']['custom_amount_text'] = [
            '#type' => 'textfield',
            '#title' => t('Custom Amount Text'),
            '#default_value' => $campaign->getCustomAmountText(),
            '#size' => 30,
            '#maxlength' => 128,
            '#required' => FALSE,
            '#description' => t('This text appears as a label below the custom amount field for set donation forms. For multi-level forms the text will appear as it\'s own level (ie button, radio, or select option).'),
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'placeholder' => array('Custom Amount'),
            ),
            '#states' => array(
                // Action to take.
                'visible' => array(
                    'input[name="form_field_options_tab[custom_amount]"]' => ['value' => 'enabled'],
                ),
            ),
        ];

    }

    /**
     * Builds the Display Option Tab form.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     */
    public function buildDisplayOptionTabForm(array &$form, FormStateInterface $form_state)
    {
        /** @var \Drupal\campaign\Entity\CampaignInterface $campaign */
        $campaign = $this->entity;
        // Display Option Tab
        $form['form_display_options_tab'] = [
            '#type' => 'details',
            '#group' => 'donation_options',
            '#title' => t('Form Display'),
            '#attributes' => [
                'class' => ['campaign-form-display-option-tab'],
            ],
            '#weight' => 20,
            '#optional' => FALSE,
        ];
        // Display Option
        $form['form_display_options_tab']['display_option'] = [
            '#type' => 'radios',
            '#title' => t('Display Options'),
            '#default_value' => $campaign->getDisplayOption(),
            '#options' => $campaign->getDefaultDisplayOptions(),
            '#description' => t('How would you like to display donation information for this form?'),
            '#required' => TRUE,
            '#attributes' => [
                'class' => ['container-inline'],
            ],
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
        ];

        // Continue Button Label
        $form['form_display_options_tab']['continue_button_label'] = [
            '#type' => 'textfield',
            '#title' => t('Continue Button'),
            '#default_value' => $campaign->getContinueButtonLabel(),
            '#size' => 30,
            '#maxlength' => 128,
            '#required' => FALSE,
            '#description' => t('The button label for displaying the additional payment fields.'),
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'placeholder' => array('Donate Now'),
            ),
            '#states' => array(
                'invisible' => array(
                    'input[name="form_display_options_tab[display_option]"]' => ['value' => 'onpage'],
                ),
            ),
        ];
        // Submit Button Label
        $form['form_display_options_tab']['checkout_button_label'] = [
            '#type' => 'textfield',
            '#title' => t('Submit Button'),
            '#default_value' => $campaign->getCheckouButtonLabel(),
            '#size' => 15,
            '#maxlength' => 128,
            '#required' => FALSE,
            '#description' => t('The button label for completing a donation.'),
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'placeholder' => ['Donate Now'],
            ]
        ];
        // Default Gateway
        $default_gateway = (is_null($campaign->getDefaultGateway())) ? 'global' : $campaign->getDefaultGateway();
        $form['form_display_options_tab']['default_gateway'] = [
            '#type' => 'select',
            '#title' => t('Default Gateway'),
            '#default_value' => $default_gateway,
            '#required' => TRUE,
            '#options' => array(
                'global' => t('Global Default'),
                'manual' => t('Test Donation'),
                'offline' => t('Offline Donation'),
            ),
            '#description' => t('By default, the gateway for this form will inherit the global default gateway (set under Donations > Settings > Payment Gateways). This option allows you to customize the default gateway for this form only.'),
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => [],
            ]
        ];
        // Guest Donations
        $guest_donations = ($campaign->getGuestDonation())?'enabled':'disabled';
        $form['form_display_options_tab']['logged_in_only'] = [
            '#type' => 'radios',
            '#title' => t('Guest Donations'),
            '#default_value' => $guest_donations,
            '#options' => array(
                'enabled' => t('Enabled'),
                'disabled' => t('Disabled'),
            ),
            '#description' => t('Do you want to allow non-logged-in users to make donations?'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ]
        ];
        // Registration
        $show_register_form = (is_null($campaign->getRegistrationForm())) ? 'both' : $campaign->getRegistrationForm();
        $form['form_display_options_tab']['show_register_form'] = [
            '#type' => 'radios',
            '#title' => t('Registration'),
            '#default_value' => $show_register_form,
            '#options' => array(
                'none' => t('None'),
                'registration' => t('Registration'),
                'registration' => t('Registration'),
                'login' => t('Login'),
                'both' => t('Registration + Login'),
            ),
            '#description' => t('Display the registration and login forms in the payment section for non-logged-in users.'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => [],
            ]
        ];
        // Floating Labels
        $floating_labels = (is_null($campaign->getFloatingLabel())) ? 'global' : $campaign->getFloatingLabel();
        $form['form_display_options_tab']['floating_labels'] = [
            '#type' => 'radios',
            '#title' => t('Floating Labels'),
            '#default_value' => $floating_labels,
            '#options' => array(
                'global' => t('Global Option'),
                'enabled' => t('Enabled'),
                'disabled' => t('Disabled'),
            ),
            '#description' => t('Select the floating labels setting for this Give form. Be aware that if you have the "Disable CSS" option enabled, you will need to style the floating labels yourself.'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ]
        ];


    }

    /**
     * Builds the Donation Goal Tab form.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     */
    public function buildDonationGoalTabForm(array &$form, FormStateInterface $form_state)
    {
        /** @var \Drupal\campaign\Entity\CampaignInterface $campaign */
        $campaign = $this->entity;
        // Display Goal Tab
        $form['form_donation_goal_tab'] = [
            '#type' => 'details',
            '#group' => 'donation_options',
            '#title' => t('Donation Goal'),
            '#attributes' => [
                'class' => ['campaign-form-donation-goal-tab'],
            ],
            '#weight' => 20,
            '#optional' => FALSE,
        ];
        
        // Donation Goal
        $donation_goal = ($campaign->getDonationGoal() == TRUE) ? 'enabled' : 'disabled';
        $form['form_donation_goal_tab']['donation_goal'] = [
            '#type' => 'radios',
            '#title' => t('Donation Goal'),
            '#default_value' => $donation_goal,
            '#options' => array(
                'enabled' => t('Enabled'),
                'disabled' => t('Disabled'),
            ),
            '#description' => t('Do you want to set a donation goal for this form?'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ]
        ];
        // Goal Amount
        $form['form_donation_goal_tab']['goal_amount'] = [
            '#type' => 'textfield',
            '#title' => t('Goal Amount'),
            '#default_value' => $campaign->getGoalAmount(),
            '#size' => 10,
            '#maxlength' => 128,
            '#required' => FALSE,
            '#description' => t('This is the monetary goal amount you want to reach for this form.'),
            '#field_prefix' => t('$'),
            '#prefix' => '<div class="field-wrap currency-symbol-field">',
            '#suffix' => '</div>',
            '#attributes' => array(
                'placeholder' => array('0.00'),
            ),
            '#states' => [
                'visible' => [
                    'input[name="form_donation_goal_tab[donation_goal]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
        // Goal Format
        $goal_format = (is_null($campaign->getGoalFormat())) ? 'amount' : $campaign->getGoalFormat();
        $form['form_donation_goal_tab']['goal_format'] = [
            '#type' => 'radios',
            '#title' => t('Goal Format'),
            '#default_value' => $goal_format,
            '#options' => array(
                'amount' => t('Amount'),
                'percentage' => t('Percentage'),
            ),
            '#description' => t('Do you want to display the total amount raised based on your monetary goal or a percentage? For instance, "$500 of $1,000 raised" or "50% funded".'),
            '#required' => FALSE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ],
            '#states' => [
                'visible' => [
                    'input[name="form_donation_goal_tab[donation_goal]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
        // Progress Bar Color
        $progress_bar_color = (is_null($campaign->getFloatingLabel())) ? '#0074bd' : $campaign->getFloatingLabel();
        $form['form_donation_goal_tab']['goal_progress_bar_color'] = [
            '#type' => 'color',
            '#title' => $this->t('Progress Bar Color'),
            '#default_value' => $progress_bar_color,
            '#description' => t('Customize the color of the goal progress bar.'),
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#states' => [
                'visible' => [
                    'input[name="form_donation_goal_tab[donation_goal]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
        // Close Form
        $close_form_config = ($campaign->getCloseFormConfig() == TRUE) ? 'enabled' : 'disabled';
        $form['form_donation_goal_tab']['close_form_when_goal_achieved'] = [
            '#type' => 'radios',
            '#title' => t('Close Form'),
            '#default_value' => $close_form_config,
            '#options' => array(
                'enabled' => t('Enabled'),
                'disabled' => t('Disabled'),
            ),
            '#description' => t('Do you want to close the donation forms and stop accepting donations once this goal has been met?'),
            '#required' => FALSE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ],
            '#states' => [
                'visible' => [
                    'input[name="form_donation_goal_tab[donation_goal]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
        // Goal Achieved Message.
        $form['form_donation_goal_tab']['goal_achieved_message'] = [
            '#type' => 'textarea',
            '#title' => t('Goal Achieved Message'),
            '#default_value' => $campaign->getGoalAchievedMessage(),
            '#required' => FALSE,
            '#description' => t('Do you want to display a custom message when the goal is closed?'),
            '#prefix' => '',
            '#suffix' => '',
            '#attributes' => [
                'placeholder' => [''],
            ],
            '#states' => [
                'visible' => [
                    'input[name="form_donation_goal_tab[donation_goal]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
    }

    /**
     * Builds the Form Content Tab form.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     */
    public function buildFormContentTabForm(array &$form, FormStateInterface $form_state)
    {
        /** @var \Drupal\campaign\Entity\CampaignInterface $campaign */
        $campaign = $this->entity;
        // Form Content Tab
        $form['form_content_tab'] = [
            '#type' => 'details',
            '#group' => 'donation_options',
            '#title' => t('Form Content'),
            '#attributes' => [
                'class' => ['campaign-form-content-tab'],
            ],
            '#weight' => 20,
            '#optional' => FALSE,
        ];
        // Display Content
        $display_content = ($campaign->getDisplayContent() == TRUE) ? 'enabled' : 'disabled';
        $form['form_content_tab']['display_content'] = [
            '#type' => 'radios',
            '#title' => t('Display Content'),
            '#default_value' => $display_content,
            '#options' => array(
                'enabled' => t('Enabled'),
                'disabled' => t('Disabled'),
            ),
            '#description' => t('Do you want to add custom content to this form?'),
            '#required' => TRUE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ],
        ];
        // Content Placement
        $display_content = (is_null($campaign->getContentPlacement())) ? 'above' : $campaign->getContentPlacement();
        $form['form_content_tab']['content_placement'] = [
            '#type' => 'radios',
            '#title' => t('Content Placement'),
            '#default_value' => $display_content,
            '#options' => [
                'above' => t('Above fields'),
                'below' => t('Below fields'),
            ],
            '#description' => t('This option controls where the content appears within the donation form.'),
            '#required' => FALSE,
            '#prefix' => '<div class="field-wrap">',
            '#suffix' => '</div>',
            '#attributes' => [
                'class' => ['container-inline'],
            ],
            '#states' => [
                'visible' => [
                    'input[name="form_content_tab[display_content]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
        // Content
        $form['form_content_tab']['form_content'] = [
            '#type' => 'textarea',
            '#title' => t('Content'),
            '#default_value' => '',
            '#required' => FALSE,
            '#description' => t('This content will display on the single give form page.'),
            '#prefix' => '',
            '#suffix' => '',
            '#attributes' => [
                'placeholder' => [''],
            ],
            '#states' => [
                'visible' => [
                    'input[name="form_content_tab[display_content]"]' => ['value' => 'enabled'],
                ],
            ],
        ];
    }

    /**
     * Builds the multi level donation form.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The attribute values form.
     */
    public function buildMultiLevelDonationForm(array &$form, FormStateInterface $form_state)
    {

        $level = $this->entity;
        $values = array();
        $user_input = $form_state->getUserInput();
        $donation_option = $form_state->getValue(['form_field_options_tab', 'donation_option']);

        // The value map allows new values to be added and removed before saving.
        // An array in the $index => $id format. $id is '_new' for unsaved values.
        $level_map = (array)$form_state->get('level_map');
        if (empty($level_map)) {
            $level_map = $values ? array_keys($values) : ['_new'];
            $form_state->set('level_map', $level_map);
        }

        $wrapper_id = Html::getUniqueId('multi-level-donations-wrapper');

        $form['form_field_options_tab']['multi_level_donation_wrapper'] = [
            '#type' => 'details',
            '#title' => $this->t('Multi Donation Levels'),
            '#open' => TRUE,
            '#states' => [
                'visible' => [ // Action to take.
                    'select[name="form_field_options_tab[donation_option]"]' => ['value' => 'multi'], // Set this element visible if the Donation Option above is Multi.
                ],
            ],
            '#prefix' => '<div id="' . $wrapper_id . '">',
            '#suffix' => '</div>',
        ];

        if ($donation_option == 'multi') {
            $form['form_field_options_tab']['multi_level_donation_wrapper']['#attributes']['class'] = ['multi-level-donation-wrapper', 'entity-meta'];
        } else {
            $form['form_field_options_tab']['multi_level_donation_wrapper']['#attributes']['class'] = ['hide', 'hidden', 'multi-level-donation-wrapper', 'entity-meta'];
        }

        $form['form_field_options_tab']['multi_level_donation_wrapper']['levels'] = [
            '#type' => 'table',
            '#tabledrag' => [
                [
                    'action' => 'order',
                    'relationship' => 'sibling',
                    'group' => 'multi-level-donation-value-order-weight',
                ],
            ],
            '#weight' => 5,
            // #input defaults to TRUE, which breaks file fields in the IEF element.
            // This table is used for visual grouping only, the element itself
            // doesn't have any values of its own that need processing.
            '#input' => FALSE,
        ];

        // Make the weight list always reflect the current number of levels.
        // Taken from WidgetBase::formMultipleElements().
        $max_weight = count($level_map);
        foreach ($level_map as $index => $id) {

            if ($id == '_new') {
                $default_weight = $max_weight;
                $remove_access = TRUE;
            } else {
                $value = $values[$id];
                $level_form['set_price']['#default_value'] = $value;
                $default_weight = $value->getWeight();
                $remove_access = $value->access('delete');
            }

            $level_form = &$form['form_field_options_tab']['multi_level_donation_wrapper']['levels'][$index];
            // The tabledrag element is always added to the first cell in the row,
            // so we add an empty cell to guide it there, for better styling.
            $level_form['#attributes']['class'][] = 'draggable';
            $level_form['tabledrag'] = [
                '#markup' => '',
            ];

            $level_form['level_set_price'] = [
                '#type' => 'textfield',
                '#title' => t('Amount'),
                '#size' => 10,
                '#maxlength' => 128,
                '#required' => FALSE,
                '#description' => t(''),
                '#field_prefix' => t('$'),
                '#prefix' => '<div class="currency-symbol-field">',
                '#suffix' => '</div>',
                '#attributes' => array(
                    'placeholder' => array('9.99'),
                ),
            ];

            $level_form['level_text'] = [
                '#type' => 'textfield',
                '#title' => t('Text'),
                '#size' => 20,
                '#maxlength' => 128,
                '#required' => FALSE,
                '#description' => t(''),
                '#prefix' => '',
                '#suffix' => '',
                '#attributes' => array(
                    'placeholder' => array('Donation Level'),
                ),
            ];

            $level_form['level_default'] = [
                '#type' => 'radio',
                '#title' => t('Default'),
            ];

            $level_form['weight'] = [
                '#type' => 'weight',
                '#title' => $this->t('Weight'),
                '#title_display' => 'invisible',
                '#delta' => $max_weight,
                '#default_value' => $default_weight,
                '#attributes' => [
                    'class' => ['multi-level-donation-value-order-weight'],
                ],
            ];

            $level_form['remove'] = [
                '#type' => 'submit',
                '#name' => 'remove_value' . $index,
                '#value' => $this->t('Remove'),
                '#limit_validation_errors' => [],
                '#submit' => ['::removeLevelSubmit'],
                '#level_index' => $index,
                '#ajax' => [
                    'callback' => '::donationOptionCallbackAjax',
                    'wrapper' => $wrapper_id,
                ],
                '#access' => $remove_access,
            ];

        }

        // Sort the values by weight. Ensures weight is preserved on ajax refresh.
        uasort($form['form_field_options_tab']['multi_level_donation_wrapper']['levels'], ['\Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);

        $form['form_field_options_tab']['multi_level_donation_wrapper']['levels']['_add_new'] = [
            '#tree' => FALSE,
        ];

        $form['form_field_options_tab']['multi_level_donation_wrapper']['levels']['_add_new']['entity'] = [
            '#type' => 'container',
            '#wrapper_attributes' => ['colspan' => 5],
        ];

        $form['form_field_options_tab']['multi_level_donation_wrapper']['levels']['_add_new']['entity']['add_level'] = [
            '#type' => 'submit',
            '#value' => $this->t('Add level'),
            '#submit' => ['::addLevelSubmit'],
            '#limit_validation_errors' => [],
            '#ajax' => [
                'callback' => '::donationOptionCallbackAjax',
                'wrapper' => $wrapper_id,
            ],
        ];

        $form['form_field_options_tab']['multi_level_donation_wrapper']['levels']['_add_new']['operations'] = [
            'data' => [],
        ];

        return $form;
    }

    /**
     * Ajax callback for donation Option operations.
     */
    public function donationOptionCallbackAjax(array $form, FormStateInterface $form_state)
    {
        return $form['form_field_options_tab']['multi_level_donation_wrapper'];
    }

    /**
     * Submit callback for adding a new level.
     */
    public function addLevelSubmit(array $form, FormStateInterface $form_state)
    {
        $form_state->setValue(['form_field_options_tab', 'donation_option'], 'multi');
        $level_map = (array)$form_state->get('level_map');
        $level_map[] = '_new';
        $form_state->set('level_map', $level_map);
        $form_state->setRebuild();
    }

    /**
     * Submit callback for removing a level.
     */
    public function removeLevelSubmit(array $form, FormStateInterface $form_state)
    {
        $form_state->setValue(['form_field_options_tab', 'donation_option'], 'multi');
        $level_index = $form_state->getTriggeringElement()['#level_index'];
        $level_map = (array)$form_state->get('level_map');
        $level_id = $level_map[$level_index];
        unset($level_map[$level_index]);
        $form_state->set('level_map', $level_map);
        // Non-new levels also need to be deleted from storage.
        if ($level_id != '_new') {
            $delete_queue = (array)$form_state->get('delete_queue');
            $delete_queue[] = $level_id;
            $form_state->set('delete_queue', $delete_queue);
        }
        $form_state->setRebuild();
    }


}
