<?php

namespace Drupal\campaign;

use Drupal\campaign\Entity\CampaignType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines the list builder for campaigns.
 */
class CampaignListBuilder extends EntityListBuilder {
  
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = t('Title'); 
    $header['type'] = t('Type');
    $header['status'] = t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    
    $row['title']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
    ] + $entity->toUrl()->toRenderArray();
    $row['type'] = campaign_get_type_label($entity);
    $row['status'] = $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished');

    return $row + parent::buildRow($entity);
  }

}
