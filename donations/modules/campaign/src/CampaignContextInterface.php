<?php

namespace Drupal\campaign;

/**
 * Holds a reference to the active store, resolved on demand.
 *
 * @see \Drupal\campaign\CampaignContext
 */
interface CampaignContextInterface {

  /**
   * Gets the active store for the current request.
   *
   * @return \Drupal\campaign\Entity\CampaignInterface
   *   The active store.
   */
  public function getCampaign();
  
}
