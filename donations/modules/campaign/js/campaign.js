/**
 * @file
 * Defines Javascript behaviors for the campaign module.
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    /**
     * Behaviors for tabs in the campaign edit form.
     *
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     *   Attaches summary behavior for tabs in the campaign edit form.
     */
    Drupal.behaviors.campaignDetailsSummaries = {
        attach: function (context) {
            var $context = $(context);

        }
    };
})(jQuery, Drupal, drupalSettings);
