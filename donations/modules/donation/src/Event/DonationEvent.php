<?php

namespace Drupal\donation\Event;

use Drupal\donation\Entity\DonationInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the donation event.
 *
 * @see \Drupal\donation\Event\DonationEvents
 */
class DonationEvent extends Event {

  /**
   * The donation.
   *
   * @var \Drupal\donation\Entity\DonationInterface
   */
  protected $donation;

  /**
   * Constructs a new DonationEvent.
   *
   * @param \Drupal\donation\Entity\DonationInterface $donation
   *   The donation.
   */
  public function __construct(DonationInterface $donation) {
    $this->donation = $donation;
  }

  /**
   * Gets the donation.
   *
   * @return \Drupal\donation\Entity\DonationInterface
   *   The donation.
   */
  public function getDonation() {
    return $this->donation;
  }

}
