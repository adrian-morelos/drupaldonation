<?php

namespace Drupal\campaign\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete a campaign type.
 */
class CampaignTypeDeleteForm extends EntityDeleteForm {
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $campaign_count = $this->entityTypeManager->getStorage('campaign')->getQuery()
      ->condition('type', $this->entity->id())
      ->count()
      ->execute();
    if ($campaign_count) {
      $caption = '<p>' . $this->formatPlural($campaign_count, '%type is used by 1 campaign on your site. You can not remove this campaign type until you have removed all of the %type campaigns.', '%type is used by @count campaigns on your site. You may not remove %type until you have removed all of the %type campaigns.', ['%type' => $this->entity->label()]) . '</p>';
      $form['#title'] = $this->getQuestion();
      $form['description'] = ['#markup' => $caption];
      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

}
