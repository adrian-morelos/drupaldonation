<?php

namespace Drupal\campaign\Cache\Context;

use Drupal\campaign\CampaignContextInterface;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines the CampaignCacheContext service, for "per campaign" caching.
 *
 * Cache context ID: 'campaign'.
 */
class CampaignCacheContext implements CacheContextInterface {

  /**
   * The campaign context.
   *
   * @var \Drupal\campaign\CampaignContextInterface
   */
  protected $campaignContext;

  /**
   * Constructs a new CampaignCacheContext class.
   *
   * @param \Drupal\campaign\CampaignContextInterface $context
   *   The campaign context.
   */
  public function __construct(CampaignContextInterface $context) {
    $this->campaignContext = $context;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Campaign');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->campaignContext->getCampaign()->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
