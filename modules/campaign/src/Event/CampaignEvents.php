<?php

namespace Drupal\campaign\Event;

final class CampaignEvents {

  /**
   * Name of the event fired after loading a store.
   *CAMPAIGN
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_LOAD = 'campaign.campaign.load';

  /**
   * Name of the event fired after creating a new store.
   *
   * Fired before the store is saved.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_CREATE = 'campaign.campaign.create';

  /**
   * Name of the event fired before saving a store.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_PRESAVE = 'campaign.campaign.presave';

  /**
   * Name of the event fired after saving a new store.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_INSERT = 'campaign.campaign.insert';
  
  /**
   * Name of the event fired after saving an existing store.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_UPDATE = 'campaign.campaign.update';

  /**
   * Name of the event fired before deleting a store.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_PREDELETE = 'campaign.campaign.predelete';

  /**
   * Name of the event fired after deleting a store.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_DELETE = 'campaign.campaign.delete';

  /**
   * Name of the event fired after saving a new store translation.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_TRANSLATION_INSERT = 'campaign.campaign.translation_insert';

  /**
   * Name of the event fired after deleting a store translation.
   *
   * @Event
   *
   * @see \Drupal\campaign\Event\CampaignEvent
   */
  const CAMPAIGN_TRANSLATION_DELETE = 'campaign.campaign.translation_delete';

}
