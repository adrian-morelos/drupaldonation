<?php

namespace Drupal\campaign\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the Fundraising campaign entity class.
 *
 * @ContentEntityType(
 *   id = "campaign",
 *   label = @Translation("Campaign"),
 *   label_singular = @Translation("campaign"),
 *   label_plural = @Translation("campaigns"),
 *   label_count = @PluralTranslation(
 *     singular = "@count campaign",
 *     plural = "@count campaigns",
 *   ),
 *   bundle_label = @Translation("Campaign type"),
 *   handlers = {
 *     "event" = "Drupal\campaign\Event\CampaignEvent",
 *     "storage" = "Drupal\donations\DonationsContentEntityStorage",
 *     "access" = "Drupal\donations\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\donations\EntityPermissionProvider",
 *     "view_builder" = "Drupal\campaign\CampaignViewBuilder",
 *     "list_builder" = "Drupal\campaign\CampaignListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\campaign\Form\CampaignForm",
 *       "add" = "Drupal\campaign\Form\CampaignForm",
 *       "edit" = "Drupal\campaign\Form\CampaignForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "translation" = "Drupal\campaign\CampaignTranslationHandler"
 *   },
 *   base_table = "campaign",
 *   data_table = "campaign_field_data",
 *   admin_permission = "administer campaign",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "campaign_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/campaign/{campaign}",
 *     "add-page" = "/campaign/add",
 *     "add-form" = "/campaign/add/{campaign_type}",
 *     "edit-form" = "/campaign/{campaign}/edit",
 *     "delete-form" = "/campaign/{campaign}/delete",
 *     "delete-multiple-form" = "/campaigns/delete",
 *     "collection" = "/admin/donations/campaigns",
 *   },
 *   bundle_entity_type = "campaign_type",
 *   field_ui_base_route = "entity.campaign_type.edit_form",
 *   common_reference_target = TRUE,
 * )
 */
class Campaign extends ContentEntityBase implements CampaignInterface
{

    use EntityChangedTrait;

    /**
     * The Minimum Default Donation Amount.
     *
     */
    protected $minimumAmount = 1.00;

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->get('title')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->set('title', $title);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->bundle();
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished()
    {
        return (bool)$this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published)
    {
        $this->set('status', (bool)$published);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner()
    {
        return $this->get('uid')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account)
    {
        $this->set('uid', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId()
    {
        return $this->get('uid')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid)
    {
        $this->set('uid', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultDonationOptions()
    {
        return [
            'set' => t('Set Donation'),
            'multi' => t('Multi-level Donation'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getDonationOption()
    {
        if (!$this->get('donation_option')->isEmpty()) {
            return $this->get('donation_option')->value;
        } else {
            $donation_options = $this->getDefaultDonationOptions();
            reset($donation_options);
            $key = key($donation_options);
            return $key;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDonationOption($donation_option)
    {
        $this->set('donation_option', $donation_option);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice()
    {
        return $this->get('set_price')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setPrice($price)
    {
        $this->set('set_price', $price);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomAmount()
    {
        return $this->get('custom_amount')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomAmount($custom_amount)
    {
        $this->set('custom_amount', $custom_amount);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMinimumAmount($amount)
    {
        $this->set('custom_amount_minimum', $amount);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMinimumAmount()
    {
        return $this->get('custom_amount_minimum')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomAmountText($text)
    {
        $this->set('custom_amount_text', $text);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomAmountText()
    {
        return $this->get('custom_amount_text')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setMultipleDonationlevels($levels)
    {
        $this->set('donation_levels', $levels);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMultipleDonationlevels()
    {
        return $this->get('donation_levels')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDisplayOption($display_option)
    {
        $this->set('display_option', $display_option);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDisplayOption()
    {
        if (!$this->get('display_option')->isEmpty()) {
            return $this->get('display_option')->value;
        } else {
            $display_options = $this->getDefaultDisplayOptions();
            reset($display_options);
            $key = key($display_options);
            return $key;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultDisplayOptions()
    {
        return [
            'onpage' => t('All Fields'),
            'modal' => t('Modal'),
            'reveal' => t('Reveal'),
            'button' => t('Button'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultCustomAmountOptions()
    {
        return [
            'enabled' => t('Enabled'),
            'disabled' => t('Disabled')
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setContinueButtonLabel($label)
    {
        $this->set('continue_button_label', $label);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getContinueButtonLabel()
    {
        return $this->get('continue_button_label')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCheckoutButtonLabel($label)
    {
        $this->set('checkout_button_label', $label);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCheckouButtonLabel()
    {
        return $this->get('checkout_button_label')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultGateway($gateway)
    {
        $this->set('default_gateway', $gateway);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultGateway()
    {
        return $this->get('default_gateway')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setGuestDonation($gateway)
    {
        $this->set('logged_in_only', $gateway);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGuestDonation()
    {
        return $this->get('logged_in_only')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setRegistrationForm($show_register_form)
    {
        $this->set('show_register_form', $show_register_form);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRegistrationForm()
    {
        return $this->get('show_register_form')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setFloatingLabel($floating_label)
    {
        $this->set('floating_label', $floating_label);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFloatingLabel()
    {
        return $this->get('floating_label')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDonationGoal($floating_label)
    {
        $this->set('donation_goal', $floating_label);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDonationGoal()
    {
        return $this->get('donation_goal')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setGoalAmount($floating_label)
    {
        $this->set('goal_amount', $floating_label);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGoalAmount()
    {
        return $this->get('goal_amount')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setGoalFormat($goal_format)
    {
        $this->set('goal_format', $goal_format);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGoalFormat()
    {
        return $this->get('goal_format')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setProgressBarColor($goal_progress_bar_color)
    {
        $this->set('goal_progress_bar_color', $goal_progress_bar_color);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProgressBarColor()
    {
        return $this->get('goal_progress_bar_color')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCloseFormConfig($close_form_config)
    {
        $this->set('close_form_when_goal_achieved', $close_form_config);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCloseFormConfig()
    {
        return $this->get('close_form_when_goal_achieved')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setGoalAchievedMessage($message)
    {
        $this->set('goal_achieved_message', $message);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGoalAchievedMessage()
    {
        return $this->get('goal_achieved_message')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDisplayContent($display_content)
    {
        $this->set('display_content', $display_content);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDisplayContent()
    {
        return $this->get('display_content')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setContentPlacement($content_placement)
    {
        $this->set('content_placement', $content_placement);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getContentPlacement()
    {
        return $this->get('content_placement')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setFormContent($content_placement)
    {
        $this->set('form_content', $content_placement);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormContent()
    {
        return $this->get('form_content')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function postSave(EntityStorageInterface $storage, $update = TRUE)
    {
        parent::postSave($storage, $update);

    }

    /**
     * {@inheritdoc}
     */
    public static function postDelete(EntityStorageInterface $storage, array $entities)
    {

    }

    /**
     * Ensures that the provided entities are in the current entity's language.
     *
     * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
     *   The entities to process.
     *
     * @return \Drupal\Core\Entity\ContentEntityInterface[]
     *   The processed entities.
     */
    protected function ensureTranslations(array $entities)
    {
        $langcode = $this->language()->getId();
        foreach ($entities as $index => $entity) {
            /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
            if ($entity->hasTranslation($langcode)) {
                $entities[$index] = $entity->getTranslation($langcode);
            }
        }
        return $entities;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['uid'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Owner'))
            ->setDescription(t('The campaign owner.'))
            ->setDefaultValueCallback('Drupal\campaign\Entity\Campaign::getCurrentUserId')
            ->setSetting('target_type', 'user')
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 50,
            ]);

        $fields['title'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Title'))
            ->setDescription(t('The campaign title.'))
            ->setRequired(TRUE)
            ->setTranslatable(TRUE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => -5,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => -5,
            ])
            ->setDisplayConfigurable('form', TRUE);

        $fields['path'] = BaseFieldDefinition::create('path')
            ->setLabel(t('URL alias'))
            ->setDescription(t('The campaign URL alias.'))
            ->setTranslatable(TRUE)
            ->setDisplayOptions('form', [
                'type' => 'path',
                'weight' => 30,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setCustomStorage(TRUE);

        $fields['status'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Published'))
            ->setDescription(t('Whether the campaign is published.'))
            ->setDefaultValue(TRUE)
            ->setTranslatable(TRUE)
            ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time when the campaign was created.'))
            ->setTranslatable(TRUE)
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayOptions('form', [
                'type' => 'datetime_timestamp',
                'weight' => 10,
            ])
            ->setDisplayConfigurable('form', TRUE);

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time when the campaign was last edited.'))
            ->setTranslatable(TRUE);

        // Donation Options Tab
        self::addDonationOptionTabBaseFieldDefinitions($fields, $entity_type);

        // Display Options Tab
        self::addDisplayOptionTabBaseFieldDefinitions($fields, $entity_type);

        // Donation Goal Tab
        self::addDonationGoalTabBaseFieldDefinitions($fields, $entity_type);

        // Form Content Tab
        self::addFormContentTabBaseFieldDefinitions($fields, $entity_type);

        return $fields;
    }

    /**
     * Add the Donation Option Base fields Definitions.
     *
     * @param array $fields
     *   An associative array containing the structure of the fields.
     * @param \Drupal\Core\Entity\EntityTypeInterface; $entity_type
     *   The current entity type.
     *
     */
    public static function addDonationOptionTabBaseFieldDefinitions(array &$fields, EntityTypeInterface $entity_type)
    {
        // Donation Option
        $fields['donation_option'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Donation Option'))
            ->setDescription(t('Do you want this form to have one set donation price or multiple levels (for example, $10, $20, $50)?'))
            ->setRequired(TRUE)
            ->setDefaultValue('set')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Set Donation
        $fields['set_price'] = BaseFieldDefinition::create('decimal')
            ->setLabel(t('Set Donation'))
            ->setDescription(t('This is the set donation amount for this form. If you have a "Custom Amount Minimum" set, make sure it is less than this amount.'))
            ->setRequired(FALSE)
            ->setDefaultValue(0)
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Custom Amount
        $fields['custom_amount'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Custom Amount'))
            ->setDescription(t('Do you want the user to be able to input their own donation amount?'))
            ->setRequired(TRUE)
            ->setDefaultValue(FALSE)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Minimum Amount
        $fields['custom_amount_minimum'] = BaseFieldDefinition::create('decimal')
            ->setLabel(t('Minimum Amount'))
            ->setDescription(t('Enter the minimum custom donation amount.'))
            ->setRequired(TRUE)
            ->setDefaultValue('1')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Custom Amount Text
        $fields['custom_amount_text'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Custom Amount Text'))
            ->setDescription(t("This text appears as a label below the custom amount field for set donation forms. For multi-level forms the text will appear as it's own level (ie button, radio, or select option)."))
            ->setRequired(TRUE)
            ->setDefaultValue('Custom Amount')
            ->setSettings([
                'default_value' => 'Custom Amount',
                'max_length' => 255,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Donation levels
        $fields['donation_levels'] = BaseFieldDefinition::create('map')
            ->setLabel(t('Multiple Donation levels'))
            ->setDescription(t('A serialized array of multiple donation levels (for example, $10, $20, $50)?'));
    }


    /**
     * Add the Display Option Base fields Definitions.
     *
     * @param array $fields
     *   An associative array containing the structure of the fields.
     * @param \Drupal\Core\Entity\EntityTypeInterface; $entity_type
     *   The current entity type.
     *
     */
    public static function addDisplayOptionTabBaseFieldDefinitions(array &$fields, EntityTypeInterface $entity_type)
    {
        // Display Option
        $fields['display_option'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Display Options'))
            ->setDescription(t('How would you like to display donation information for this form?'))
            ->setRequired(TRUE)
            ->setDefaultValue('onpage')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Continue Button Label
        $fields['continue_button_label'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Continue Button'))
            ->setDescription(t('The button label for displaying the additional payment fields.'))
            ->setRequired(FALSE)
            ->setDefaultValue('Donate Now')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Submit Button Label
        $fields['checkout_button_label'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Continue Button'))
            ->setDescription(t('The button label for completing a donation.'))
            ->setRequired(FALSE)
            ->setDefaultValue('Donate Now')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Default Gateway
        $fields['default_gateway'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Default Gateway'))
            ->setDescription(t('By default, the gateway for this form will inherit the global default gateway (set under Donations > Settings > Payment Gateways). This option allows you to customize the default gateway for this form only.'))
            ->setRequired(FALSE)
            ->setDefaultValue('global')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Guest Donations
        $fields['logged_in_only'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Guest Donations'))
            ->setDescription(t('Do you want to allow non-logged-in users to make donations?'))
            ->setRequired(TRUE)
            ->setDefaultValue(FALSE)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Registration
        $fields['show_register_form'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Registration'))
            ->setDescription(t('Display the registration and login forms in the payment section for non-logged-in users.'))
            ->setRequired(TRUE)
            ->setDefaultValue('both')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Floating Labels
        $fields['floating_label'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Floating Labels'))
            ->setDescription(t('Select the floating labels setting for this Give form. Be aware that if you have the "Disable CSS" option enabled, you will need to style the floating labels yourself.'))
            ->setRequired(TRUE)
            ->setDefaultValue('enabled')
            ->setSetting('max_length', 128)
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
    }

    /**
     * Add the Donation Goal Base fields Definitions.
     *
     * @param array $fields
     *   An associative array containing the structure of the fields.
     * @param \Drupal\Core\Entity\EntityTypeInterface; $entity_type
     *   The current entity type.
     *
     */
     public static function addDonationGoalTabBaseFieldDefinitions(array &$fields, EntityTypeInterface $entity_type){
         // Donation Goal
         $fields['donation_goal'] = BaseFieldDefinition::create('boolean')
             ->setLabel(t('Donation Goal'))
             ->setDescription(t('Do you want to set a donation goal for this form?'))
             ->setRequired(TRUE)
             ->setDefaultValue(FALSE)
             ->setDisplayConfigurable('form', TRUE)
             ->setDisplayConfigurable('view', TRUE);
         // Goal Amount
         $fields['goal_amount'] = BaseFieldDefinition::create('decimal')
             ->setLabel(t('Goal Amount'))
             ->setDescription(t('This is the monetary goal amount you want to reach for this form.'))
             ->setRequired(FALSE)
             ->setDefaultValue(0)
             ->setDisplayConfigurable('form', TRUE)
             ->setDisplayConfigurable('view', TRUE);
         // Goal Format
         $fields['goal_format'] = BaseFieldDefinition::create('string')
             ->setLabel(t('Goal Format'))
             ->setDescription(t('Do you want to display the total amount raised based on your monetary goal or a percentage? For instance, "$500 of $1,000 raised" or "50% funded".'))
             ->setRequired(FALSE)
             ->setDefaultValue('amount')
             ->setDisplayConfigurable('form', TRUE)
             ->setDisplayConfigurable('view', TRUE);
         // Progress Bar Color
         $fields['goal_progress_bar_color'] = BaseFieldDefinition::create('string')
             ->setLabel(t('Progress Bar Color'))
             ->setDescription(t('Customize the color of the goal progress bar.'))
             ->setRequired(FALSE)
             ->setDefaultValue('#0074bd')
             ->setDisplayConfigurable('form', TRUE)
             ->setDisplayConfigurable('view', TRUE);
         // Close Form
         $fields['close_form_when_goal_achieved'] = BaseFieldDefinition::create('boolean')
             ->setLabel(t('Close Form'))
             ->setDescription(t('Do you want to close the donation forms and stop accepting donations once this goal has been met?'))
             ->setRequired(FALSE)
             ->setDefaultValue(FALSE)
             ->setDisplayConfigurable('form', TRUE)
             ->setDisplayConfigurable('view', TRUE);
         // Goal Achieved Message.
         $fields['goal_achieved_message'] = BaseFieldDefinition::create('string')
             ->setLabel(t('Goal Achieved Message'))
             ->setDescription(t('Do you want to display a custom message when the goal is closed?'))
             ->setRequired(FALSE)
             ->setDefaultValue('')
             ->setDisplayConfigurable('form', TRUE)
             ->setDisplayConfigurable('view', TRUE);
     }

    /**
     * Add the Form Content Base fields Definitions.
     *
     * @param array $fields
     *   An associative array containing the structure of the fields.
     * @param \Drupal\Core\Entity\EntityTypeInterface; $entity_type
     *   The current entity type.
     *
     */
    public static function addFormContentTabBaseFieldDefinitions(array &$fields, EntityTypeInterface $entity_type){
        // Display Content
        $fields['display_content'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Display Content'))
            ->setDescription(t('Do you want to add custom content to this form?'))
            ->setRequired(TRUE)
            ->setDefaultValue('enabled')
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Content Placement
        $fields['content_placement'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Content Placement'))
            ->setDescription(t('This option controls where the content appears within the donation form.'))
            ->setRequired(FALSE)
            ->setDefaultValue('above')
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        // Content
        $fields['form_content'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Content'))
            ->setDescription(t('This content will display on the single give form page.'))
            ->setRequired(FALSE)
            ->setDefaultValue('')
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
    }

    /**
     * Default value callback for 'uid' base field definition.
     *
     * @see ::baseFieldDefinitions()
     *
     * @return array
     *   An array of default values.
     */
    public static function getCurrentUserId()
    {
        return [\Drupal::currentUser()->id()];
    }

}

