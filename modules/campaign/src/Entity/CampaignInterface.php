<?php

namespace Drupal\campaign\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Defines the interface for campaign.
 *
 */
interface CampaignInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface
{

    /**
     * Gets the campaign type.
     *
     * @return string
     *   The campaign type.
     */
    public function getType();

    /**
     * Gets the campaign title.
     *
     * @return string
     *   The campaign title.
     */
    public function getTitle();

    /**
     * Sets the campaign title.
     *
     * @param string $title
     *   The campaign title.
     *
     * @return $this
     */
    public function setTitle($title);

    /**
     * Gets the campaign creation timestamp.
     *
     * @return int
     *   Creation timestamp of the campaign.
     */
    public function getCreatedTime();

    /**
     * Sets the campaign creation timestamp.
     *
     * @param int $timestamp
     *   The campaign creation timestamp.
     *
     * @return \Drupal\campaign\NodeInterface
     *   The called campaign entity.
     */
    public function setCreatedTime($timestamp);

    /**
     * Returns the campaign published status indicator.
     *
     * Unpublished campaigns are only visible to their authors and to administrators.
     *
     * @return bool
     *   TRUE if the campaign is published.
     */
    public function isPublished();

    /**
     * Sets the published status of a campaign..
     *
     * @param bool $published
     *   TRUE to set this campaign to published, FALSE to set it to unpublished.
     *
     * @return \Drupal\campaign\NodeInterface
     *   The called campaign entity.
     */
    public function setPublished($published);

    /**
     * Gets the Donation option.
     *
     * @return string
     *   The donation option.
     */
    public function getDonationOption();

    /**
     * Sets the campaign donation option.
     *
     * @param string $donation_option
     *   The campaign donation option.
     *
     * @return $this
     */
    public function setDonationOption($donation_option);

}