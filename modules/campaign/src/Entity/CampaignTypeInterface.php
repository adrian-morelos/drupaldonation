<?php

namespace Drupal\campaign\Entity;

use Drupal\donations\Entity\DonationsBundleEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for campaign types.
 *
 */
interface CampaignTypeInterface extends DonationsBundleEntityInterface, EntityDescriptionInterface {
   
  /**
   * Determines whether the campaign type is locked.
   *
   * @return string|false
   *   The module name that locks the type or FALSE.
   */
  public function isLocked();

  /**
   * Gets the help information.
   *
   * @return string
   *   The help information of this campaign type.
   */
  public function getHelp();

  /** 
   * Gets the description.
   *
   * @return string
   *   The description of this campaign type.
   */
  public function getDescription();
  
}
