<?php

namespace Drupal\campaign\Entity;

use Drupal\donations\Entity\DonationsBundleEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\campaign\Entity\CampaignTypeInterface;
/**
 * Defines the campaign type entity class.
 *
 * @ConfigEntityType(
 *   id = "campaign_type",
 *   label = @Translation("Campaign type"),
 *   label_singular = @Translation("Campaign type"),
 *   label_plural = @Translation("Campaign types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Campaign type",
 *     plural = "@count Campaign types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\campaign\CampaignTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\campaign\Form\CampaignTypeForm",
 *       "edit" = "Drupal\campaign\Form\CampaignTypeForm",
 *       "delete" = "Drupal\campaign\Form\CampaignTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer campaign_type",
 *   config_prefix = "campaign_type",
 *   bundle_of = "campaign",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "label", 
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/donations/config/campaign-types/add",
 *     "edit-form" = "/admin/donations/config/campaign-types/{campaign_type}/edit",
 *     "delete-form" = "/admin/donations/config/campaign-types/{campaign_type}/delete",
 *     "collection" = "/admin/donations/config/campaign-types",
 *   },
 *   config_export = {
 *     "label",
 *     "type",
 *     "description",
 *     "help",
 *     "traits",
 *   },
 * )
 */
class CampaignType extends DonationsBundleEntityBase implements CampaignTypeInterface {
  
  /**
   * The machine name of this campaign type.
   *
   * @var string
   *
   * @todo Rename to $id.
   */
  protected $type;

  /**
   * The human-readable name of the campaign type.
   *
   * @var string
   *
   * @todo $label.
   */
  protected $label;

  /**
   * A brief description of this campaign type.
   *
   * @var string
   */
  protected $description;

  /**
   * Help information shown to the user when creating a Node of this type.
   *
   * @var string
   */
  protected $help;
  
  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }
   
  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('campaign_type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelp() {
    return $this->help;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}