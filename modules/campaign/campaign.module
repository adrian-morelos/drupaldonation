<?php

/**
 * @file
 * Defines the Fundraising campaign entity and associated features
 *
 * Modules and scripts may programmatically submit campaigns using the usual form
 * API pattern.
 */

use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\ConfigurableLanguageInterface;
use Drupal\campaign\Entity\Campaign;
use Drupal\campaign\Entity\CampaignType;
use Drupal\campaign\Entity\CampaignInterface;
use Drupal\campaign\Entity\CampaignTypeInterface;

/**
 * Implements hook_theme().
 */
function campaign_theme() {
  return [
    'campaign_form' => [
      'render element' => 'form',
    ],
    'campaign' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Returns the campaign type label for the passed campaign.
 *
 * @param \Drupal\campaign\CampaignInterface $campaign
 *   A campaign entity to return the campaign type's label for.
 *
 * @return string|false
 *   The campaign type label or FALSE if the campaign type is not found.
 *
 * @todo Add this as generic helper method for config entities representing
 *   entity bundles.
 */
function campaign_get_type_label(CampaignInterface $campaign) { 
  $type = CampaignType::load($campaign->bundle());
  return $type ? $type->label() : FALSE;
}

/**
 * Prepares variables for campaign templates.
 *
 * Default template: campaign.html.twig.
 *
 * Most themes use their own copy of campaign.html.twig. The default is located
 * inside "/donations/modules/campaign/templates/campaign.html.twig". Look in there for the
 * full list of variables.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An array of elements to display in view mode.
 *   - campaign: The campaign object.
 *   - view_mode: View mode; e.g., 'full', 'teaser', etc.
 */
function template_preprocess_campaign(&$variables) {
    /** @var \Drupal\node\NodeInterface $node */
    $campaign = $variables['elements']['#campaign'];
    ksm($campaign);
}