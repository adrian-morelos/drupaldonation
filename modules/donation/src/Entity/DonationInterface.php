<?php

namespace Drupal\donation\Entity;

use Drupal\commerce_store\Entity\EntityStoresInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for donations.
 */
interface DonationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityStoresInterface {
	
  /**
   * Gets the donation title.
   *
   * @return string
   *   The donation title
   */
  public function getTitle();

  /**
   * Sets the donation title.
   *
   * @param string $title
   *   The donation title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Get whether the donation is published.
   *
   * Unpublished donations are only visible to their authors and administrators.
   *
   * @return bool
   *   TRUE if the donation is published, FALSE otherwise.
   */
  public function isPublished();

  /**
   * Sets whether the donation is published.
   *
   * @param bool $published
   *   Whether the donation is published.
   *
   * @return $this
   */
  public function setPublished($published);

  /**
   * Gets the donation creation timestamp.
   *
   * @return int
   *   The donation creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the donation creation timestamp.
   *
   * @param int $timestamp
   *   The donation creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
