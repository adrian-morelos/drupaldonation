<?php

namespace Drupal\donation\Event;

final class DonationEvents {

  /**
   * Name of the event fired after loading a donation.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_LOAD = 'donation.donation.load';

  /**
   * Name of the event fired after creating a new donation.
   *
   * Fired before the donation is saved.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_CREATE = 'donation.donation.create';

  /**
   * Name of the event fired before saving a donation.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_PRESAVE = 'donation.donation.presave';

  /**
   * Name of the event fired after saving a new donation.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_INSERT = 'donation.donation.insert';

  /**
   * Name of the event fired after saving an existing donation.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_UPDATE = 'donation.donation.update';

  /**
   * Name of the event fired before deleting a donation.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_PREDELETE = 'donation.donation.predelete';

  /**
   * Name of the event fired after deleting a donation.
   *
   * @Event
   *
   * @see \Drupal\donation\Event\DonationEvent
   */
  const DONATION_DELETE = 'donation.donation.delete';

}